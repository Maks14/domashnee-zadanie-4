<?php 
$data = file_get_contents(__DIR__ . 'data-phone.json');
$dataPhone = json_decode($data, true);
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>table</title>
</head>
<body>
  <table>
    <tr>
      <td>Имя</td>
      <td>Фамилия</td>
      <td>Адрес</td>
      <td>Номер телефона</td>
    </tr>
    <?php foreach ($dataPhone as $value) { ?>
    <tr>
      <td><?php echo $value['firstName'] ?></td>
      <td><?php echo $value['lastName'] ?></td>
      <td><?php echo $value['address'] ?></td>
      <td><?php echo $value['phoneNumber'] ?></td>
    </tr> <?php } ?>
  </table>
</body>
</html>